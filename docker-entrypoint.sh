#!/bin/sh

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

# Start the utility
python garten.py
