#!/usr/bin/env python
"""Waits for chronicler.
"""

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

# Waits for Chronicler.
#
# This simply waits until Chronicler is responding
# on it's outbound path where we expect a 400 (no runner key)
# or 401 (invalid runner key).

import os
import sys
import time

import requests

# Get secrets (all must be set)
chronicler_url = os.environ.get('GARTEN_CHRONICLER_URL')
chronicler_resource = os.environ.get('GARTEN_CHRONICLER_RESOURCE')
if chronicler_url is None or chronicler_resource is None:
    print("ERROR: Not all environment variables were set.")
    sys.exit(10)

# Chronicler POST timeout
_CHRONICLER_POST_TIMEOUT_S = 2.0

# Look for the Chronicler (using the resource path)...
chronicler_outbound_url = chronicler_url + '/' + chronicler_resource
print(f"Waiting for Chronicler at '{chronicler_outbound_url}'...")
FOUND = False
while not FOUND:

    EXCEPTION = False
    try:
        resp = requests.post(chronicler_outbound_url,
                             timeout=_CHRONICLER_POST_TIMEOUT_S)
    except:  # pylint: disable=bare-except
        print("- Exception finding Chronicler.")
        EXCEPTION = True

    if not EXCEPTION:
        if resp is not None and resp.status_code in [201, 400, 401]:
            FOUND = True
        else:
            print(f'- Failed ({resp})')

    if not FOUND:
        time.sleep(10.0)

# Done
print("Found!")
