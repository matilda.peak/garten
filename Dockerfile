# -----------------------------------------------------------------------------
# Copyright (C) 2022 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

ARG from_image=python:3.9.12-alpine3.15
FROM ${from_image}

# Labels
LABEL maintainer='Matilda Peak <info@matildapeak.com>'

# Install timezone info into Alpine image...
RUN apk add --no-cache \
    build-base \
    tzdata
ENV TZ=UTC

# Force the binary layer of the stdout and stderr streams
# to be unbuffered
ENV PYTHONUNBUFFERED 1

# Base directory for the application
# Also used for user directory
ENV APP_ROOT /home/garten

# Containers should NOT run as root
# (as good practice)
# See https://github.com/mhart/alpine-node/issues/48
RUN addgroup -g 6000 -S garten && \
    adduser -u 6000 -S -D -h ${APP_ROOT} -s /bin/sh garten -G garten && \
    chown -R garten.garten ${APP_ROOT}

WORKDIR ${APP_ROOT}

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY garten.py ./
COPY bikepoint/ ./bikepoint/
COPY image-config/logging.yml ./
COPY LICENCE.txt ./
COPY docker-entrypoint.sh ./
COPY docker-healthcheck.sh ./
COPY chronicler_wait.py ./

RUN chmod 755 docker-entrypoint.sh && \
    chmod 755 docker-healthcheck.sh && \
    chmod 755 chronicler_wait.py && \
    chmod 777 ${APP_ROOT} && \
    chown -R garten.garten ${APP_ROOT}

USER 6000
ENV HOME ${APP_ROOT}

CMD ./docker-entrypoint.sh
