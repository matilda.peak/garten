---
Title:      A Chronicler Data Feed (Garten)
Author:     Alan Christie
Date:       12 May 2022
Copyright:  Matilda Peak. All rights reserved.
---

[![build status](https://gitlab.com/matilda.peak/garten/badges/master/build.svg)](https://gitlab.com/matilda.peak/garten/commits/master)

![Ansible Role](https://img.shields.io/ansible/role/42344)

# The Garten project
A feed of real-world real-time data for use with Chronicler
experiments. This early implementation collects [TFL API] BikePoint data.

## Running Garten
Garten is distributed as a container image but you can run it from the project
root after setting up a few things...

1.  You must have Python 3.9
1.  Ideally, create a Python [VirtualEnv] with something like
    `python -m venv venv`. You'll automatically be put into the
    new environment.
1.  Install the Python run-time requirements with
    `pip install -r requirements.txt`
1.  Copy `setenv-template.sh` to `setenv.sh` (this new file will be ignored
    by Git) and set your TfL credentials.
1.  Run `source setenv.sh`
1.  Run Garten with `./garten.py`

>   You can disable the cache using the `setenv.sh` script. The cache is
    simply used to limit messages to just changes. You can also set the
    collection period (default is 5 minutes).

## Environment variables
You will need to define:

-   `TFL_APP_ID` (your TfL application ID)
-   `TFL_APP_KEY` (your TfL application key)
-   `GARTEN_CHRONICLER_URL`

The collection period has a default value (typically 5 minutes) but
it can be set using the following environment variable:

-   `GARTEN_COLLECTION_PERIOD_M`
 
To experiment with collection to an SQLite database, useful for offline
collection and storage of data outside of a container image for your
own analysis, you can set:

-   `GARTEN_USE_DB` to `Yes`

To avoid using a BikeCache, which allows only the changes ot be sent,
you can set an environment variable.

-   `GARTEN_USE_BIKE_CACHE` to `No`

## TFL BikePoint API Records
The TFL API method `https://api.tfl.gov.uk/BikePoint` returns an array of
dictionary items, one for each BikePoint location. One item is reproduced
below for reference where the `$type` indicates that it is information about
a _Place_. 

    {
        "$type": "Tfl.Api.Presentation.Entities.Place, Tfl.Api.Presentation.Entities",
        "id": "BikePoints_1",
        "url": "/Place/BikePoints_1",
        "commonName": "River Street , Clerkenwell",
        "placeType": "BikePoint",
        "additionalProperties": [
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "TerminalName",
            "sourceSystemKey": "BikePoints",
            "value": "001023",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "Installed",
            "sourceSystemKey": "BikePoints",
            "value": "true",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "Locked",
            "sourceSystemKey": "BikePoints",
            "value": "false",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "InstallDate",
            "sourceSystemKey": "BikePoints",
            "value": "1278947280000",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "RemovalDate",
            "sourceSystemKey": "BikePoints",
            "value": "",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "Temporary",
            "sourceSystemKey": "BikePoints",
            "value": "false",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "NbBikes",
            "sourceSystemKey": "BikePoints",
            "value": "1",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "NbEmptyDocks",
            "sourceSystemKey": "BikePoints",
            "value": "18",
            "modified": "2017-10-30T13:24:44.387Z"
          },
          {
            "$type": "Tfl.Api.Presentation.Entities.AdditionalProperties, Tfl.Api.Presentation.Entities",
            "category": "Description",
            "key": "NbDocks",
            "sourceSystemKey": "BikePoints",
            "value": "19",
            "modified": "2017-10-30T13:24:44.387Z"
          }
        ],
        "children": [],
        "childrenUrls": [],
        "lat": 51.529163,
        "lon": -0.10997
    }

Significant fields for the _Place_ are:

-   `id` representing the unique place reference
-   `commonName` representing the command name for the place
-   `lat` and `lon` for the coordinates of the location

Within the `addtionalProperties` array for the _Place_ there are fields for:

-   `key = NbBikes` for the number of bikes currently available
-   `key = NbEmptyDocks` for the number of empty docks
-   `key = NbDocks` for the total number of docks at the location

>   A mismatch in these numbers i.e. where
    `nbDocks - (nbBikes + nbSpaces) > 0` indicates broken docks.
    The `modified` field the `NbBikes` property is a record of when the
    number of bikes at the location changed.

## Container Building
There's a Dockerfile to build the Docker image. Main building is done
via the GitLab CI service (see `.gitlab-ci.yml`).

---

[TfL API]: https://api.tfl.gov.uk
[VirtualEnv]: https://virtualenvwrapper.readthedocs.io/en/latest/
