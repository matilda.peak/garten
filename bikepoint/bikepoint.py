#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""Garten. A TfL bike-point availability data collector for Chronicler.

You will need to set some environment variables before you can run this code.
They are the credentials for your TfL API registration and must be provided:

-   TFL_APP_ID
-   TFL_APP_KEY

Additional, optional environment variables include the following:

-   GARTEN_USE_DB   If this environment variable is set the code
                    will write collected results to an SQLite DB in the
                    current directory.
-   GARTEN_COLLECTION_PERIOD_M  This defines the collection period. If not
                                set a default (typically 5 minutes) is used.
                                The value must not be greater than 60 and
                                must be a divisor of 60.
-   GARTEN_USE_BIKE_CACHE   Unless this is not set to 'False', 'No' or 'N' then
                            the code uses a file-based cache to limit the
                            emitted messages to only those that represent
                            changes of bike numbers at each location. The code
                            uses the Python shelve module, writing its cache to
                            a '.shelf' file in the current working directory.
-   GARTEN_CHRONICLER_URL   If set this is used as the base of the Chronicler
                            URL with bike data being transmitted to it. If not
                            set Chronicler is not used.

On start-=up the `run()` method pauses in order to synchronise the collection
of data, where the rate (in minutes) is defined in `_COLLECTION_PERIOD_M`,
so that collections occur on the hour. When time is right an initial call to
the TfL API is made via `_get_data()` (to collect the first set of BikePoint
data items) before handing further calls to `_get_data()` over to a
background scheduler. Data collection is synchronised so that a collection
occurs on the hour - i.e. a 10 minute interval will result in data collection
at 07:00, 07:10, 07:20 etc.

`_get_data()` calls the TfL API and then simply unpacks the returned data,
sending each BikePoint record to the `_dispatch()` method vis a Queue for
processing and optional delivery to Chronicler.

`_dispatch()` pulls objects from the Queue and passes them onto Chronicler,
optionally printing the received data and storing it into a SQLite database.

Data is optionally cached so that the messages sent on to Chronicler are
limited to those with information about sites where the number available
bikes has changed.

If an optional local database is to be populated the `USE_DB_ENV` variable
(owned by `initialise_db.py`) will be set. If using the DB three database
tables are maintained:

-   A `Collection` table that records each API call's start time and duration.
-   A `Location` table that maintains a list of the various BikePoint locations
    (there were around 800 at the time of writing).
-   A `Dock` table that records the number of bikes, spaces and broken docks
    for each location. The Dock table is updated at the collection interval
    with locations where the number of available bikes has changed.

The tables are defined in `initialise_db.py` on which we depend.
"""

import json
import os
import shelve
import time
import queue
from datetime import datetime, timedelta
import logging
from logging.config import dictConfig
from threading import Thread
import signal
import requests

from pytz import timezone

import yaml

from apscheduler.schedulers.background import BackgroundScheduler

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

from matildapeak.chronicler_transmitter import ChroniclerTransmitter

from bikepoint.initialise_db import BASE, Collection, Dock, Location, PlaceType
from bikepoint.initialise_db import DB, USE_DB_ENV
from bikepoint.error import write_error

# Load logger configuration (from cwd)...
# But only if the logging configuration is present!
_LOGGING_CONFIG_FILE = 'logging.yml'
if os.path.isfile(_LOGGING_CONFIG_FILE):
    _LOGGING_CONFIG = None
    with open(_LOGGING_CONFIG_FILE, 'r', encoding='utf8') as stream:
        try:
            _LOGGING_CONFIG = yaml.load(stream, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)
    dictConfig(_LOGGING_CONFIG)

# Now it's configured
# (and it should be for all modules as `disable_existing_loggers`
# is set to False in `logging.yml`)
# we can now create our own logger. We use 'runner' as the logger here
# otherwise we'll get __main__ when we're run. We can use __main__ for all
# modules that are imported.
_LOGGER = logging.getLogger('bikepoint')

# The API and resource
_API = 'https://api.tfl.gov.uk'
_RESOURCE = 'BikePoint'
# The BikePoint 'id' property prefix.
# All BikePoint locations start with this string...
_BIKE_POINT_ID_PREFIX = 'BikePoints_'

_APP_ID = os.environ['TFL_APP_ID']
_APP_KEY = os.environ['TFL_APP_KEY']

_TZ = timezone('Europe/London')

_AUTH = {'app_id': _APP_ID,
         'app_key': _APP_KEY}

_USE_DB = os.environ.get(USE_DB_ENV)

# Collection rate (via the environment)
_DEFAULT_COLLECTION_PERIOD_M = '5'
_COLLECTION_PERIOD_M_STR = os.environ.get('GARTEN_COLLECTION_PERIOD_M',
                                          _DEFAULT_COLLECTION_PERIOD_M)
_COLLECTION_PERIOD_M = int(_COLLECTION_PERIOD_M_STR)

# Collection period must not greater than 60.
# and must be a divisor of 60. i.e. 2, 3, 4
assert _COLLECTION_PERIOD_M <= 60
assert _COLLECTION_PERIOD_M * (60 // _COLLECTION_PERIOD_M) == 60

# The name of the bike occupancy cache.
# A shelf database of locations and the current number of bikes.
# Used to limit the number of messages sent to Chronicler.
_OCCUPANCY_CACHE_NAME = 'bike-cache.shelf'
# Whether to cache (via the environment)
# Yes by default.
_USE_BIKE_CACHE = True
_USE_BIKE_CACHE_VALUE = os.environ.get('GARTEN_USE_BIKE_CACHE')
if _USE_BIKE_CACHE_VALUE and\
                _USE_BIKE_CACHE_VALUE.lower() in ['no', 'n', 'false']:
    _USE_BIKE_CACHE = False

# Key for additional properties in the BikePoint data
_ADDITIONAL_PROP_KEY = 'additionalProperties'

# The dispatch queue.
# Contains dictionary objects that are sent to Chronicler.
# Items are put into the queue by the APScheduler-driven _get_data() method
# and are pulled from the queue in the _dispatch() method.
_DISPATCH_QUEUE = queue.Queue()

# The Chronicler URL (optional).
# If not set we won't send to Chronicler.
#
# Early tests seem to indicate that we're looking at about
# 10 messages/second to Chronicler. therefore, to transmit data for 800
# locations allow about 80 to 90 seconds. Assume a realistic worse-case
# max polling rate of once every 3 to 5 minutes.
_CHRONICLER_URL = os.environ.get('GARTEN_CHRONICLER_URL')
# Chronicler resource
_CHRONICLER_RESOURCE = os.environ.get('GARTEN_CHRONICLER_RESOURCE', None)
if not _CHRONICLER_RESOURCE:
    _CHRONICLER_RESOURCE = os.path.join('inbound',
                                        'customer',
                                        'domain',
                                        'default',
                                        'curl',
                                        'test-event')
# Our assigned application ID and secret code.
# Without these we cannot communicate with Chronicler.
_CHRONICLER_APP_ID = os.environ.get('GARTEN_CHRONICLER_APP_ID', 'app1234')
_CHRONICLER_APP_CODE = os.environ.get('GARTEN_CHRONICLER_APP_CODE', 'code1234')

# The longest API call duration.
# Logged for information when it changes.
# Start with 4 seconds (not interested unless it's above this)
#
# The longest API call noticed so far was:
#   13.75 seconds
_LONGEST_API_DURATION = timedelta(seconds=4.0)

# A short sleep period (seconds)
_SHORT_SLEEP_S = 3.0

# Keep the main-loop running...
_RUN = True

if _USE_DB:

    # Setup database variables...
    _ENGINE = create_engine(DB)
    BASE.metadata.bind = _ENGINE
    _SESSION = scoped_session(sessionmaker(bind=_ENGINE))


def _signal_handler(signum, frame):
    """A signal handler, to handle things like CTRL-C.
    """
    # pylint: disable=global-statement
    global _RUN

    assert signum
    assert frame

    if _RUN:
        # All we have to do is clear the _RUN flag
        # which will release the _dispatch() thread.
        _LOGGER.warning('Caught signal (%s). Clearing _RUN...', signum)
        _RUN = False


def _dispatch(dispatch_queue, bike_cache=None, chronicler=None):
    """Used as a thread.

    We're given dictionary items from the _get_data() method through a queue.

    We process the collected data on a thread separate to the scheduled
    collection thread. here we can write to a DB and pass the data onto
    Chronicler without risking blocking the scheduled thread.

    If using a database we also populate the `Location` and `Dock` tables with
    the details of each new location and the data for the bikes at each site.

    :param bike_cache: A cache of bike numbers at each site (or None)
    :type bike_cache: ``shelve``
    :param dispatch_queue: The queue through which BikePoint data is sent to us
    :type dispatch_queue: ``Queue``
    :param chronicler: A Chronicler instance, or None
    :type chronicler: ``Chronicler``
    """

    # The last collection record sent by _get_data().
    # The first thing we receive should be a collection record.
    collection_record = None
    # Record of the time the last bike was returned or take.
    # i.e. the last change in bike availability.
    # A datetime.
    time_of_last_bike_change = None
    # The numeric ID of the last location.
    # Ass this increases with each record in a given collection
    # we know we've started to receive a new block of data
    # when this wraps
    last_location_uid = 0
    # And the number of location bike availability changes
    # since the location wrapped...
    changes_since_last_collection = 0

    _LOGGER.info('In dispatch...')

    if _USE_DB:

        # Create a DB session
        # to record the BilePoint records we'll be receiving.
        db_session = _SESSION()

    while _RUN:

        try:
            obj = dispatch_queue.get(timeout=_SHORT_SLEEP_S)
        except queue.Empty:
            # Timeout - an opportunity to inspect _RUN
            # and make sure we're still needed...
            continue

        # Blocks of BikePoint data from the same collection
        # begin with a Collection object. If found, process it and
        # continue
        if isinstance(obj, Collection):
            collection_record = obj
            _LOGGER.debug('Handled Collection record (%s, %s)',
                          obj.timestamp, obj.latency_s)
            continue

        # Must be a dictionary
        assert isinstance(obj, dict)

        record_type = obj['$type']
        if not record_type.startswith('Tfl.Api.Presentation.Entities.Place'):
            _LOGGER.warning('Type is not a Place (%s)', record_type)
            continue

        # Ignore records that are not `BikePoint`
        place_type = obj['placeType']
        if place_type not in ['BikePoint']:
            _LOGGER.warning('Given unsupported place type (%s)', place_type)
            continue

        # To save DB space let's convert the BikePoint location to a number.
        # It (conveniently) already contains a number, after a
        # _BIKE_POINT_ID_PREFIX.
        uid_str = obj['id']
        if not uid_str.startswith(_BIKE_POINT_ID_PREFIX):
            _LOGGER.warning('Given odd-looking BikePoint location id (%s)',
                            uid_str)
            continue
        # Out uid is the numeric form of the characters
        # that follow the prefix.
        try:
            uid = int(uid_str[len(_BIKE_POINT_ID_PREFIX):])
        except ValueError:
            _LOGGER.warning('Given non-numeric BikePoint location id (%s)',
                            uid_str)
            continue

        common_name = obj['commonName']
        docks = -1
        spaces = -1
        bikes = -1
        modified_str = None

        # Clean-up the common-name (some need it)
        # - Spaces before commas
        # - trim
        common_name = common_name.replace(' ,', ',')
        common_name = common_name.strip()

        location_record = None
        if _USE_DB:
            # Is this BikePoint for a new (unknown) Location?
            # If the location is not in the location table
            # then add it...
            location_record = db_session.query(Location)\
                .filter(Location.uid == uid).first()
            if not location_record:

                # Insert new BikePoint location information
                # into the location table...
                new_location = Location(uid=uid,
                                        common_name=common_name,
                                        place_type=PlaceType.BIKE_POINT,
                                        lat=obj['lat'],
                                        lon=obj['lon'],
                                        collection=collection_record)
                db_session.add(new_location)
                db_session.commit()

            # Get the (new?) location record...
            location_record = db_session.query(Location)\
                .filter(Location.uid == uid).first()

        # Get bike information out of the `additionalProperties`
        # for this record. The 'modified_str' is taken from the
        # time the bike count changed.
        for additional_data in obj[_ADDITIONAL_PROP_KEY]:
            if additional_data['key'] == 'NbDocks':
                docks = int(additional_data['value'])
            elif additional_data['key'] == 'NbEmptyDocks':
                spaces = int(additional_data['value'])
            if additional_data['key'] == 'NbBikes':
                bikes = int(additional_data['value'])
                modified_str = additional_data['modified']

        # If the number of docks, spaces or bikes cannot be found
        # then we have a record with incomplete data...
        if docks < 0 or spaces < 0 or bikes < 0:

            dump_obj = False
            if not obj[_ADDITIONAL_PROP_KEY]:
                msg = f'Empty {_ADDITIONAL_PROP_KEY}'
            else:
                msg = 'Missing dock data'
                dump_obj = True

            # One crucial field is not in the response.
            # Can't properly analyse this bike-point so log the
            # values we've got so far and then dump the received object.
            _LOGGER.warning('%s for "%s" (%s, %s, %s)',
                            msg, common_name, docks, bikes, spaces)
            if dump_obj:
                _LOGGER.warning('Received ... %s', obj)

            continue

        # Update the date/time of the last change in availability.
        #
        # Compensate for TfL date string having variable length!
        # The milliseconds field is not always 3 digits!
        # So we just take the fractions away...
        # to form something like '2014-00-00T00:00:00Z'.
        modified_str = modified_str[0:19] + 'Z'
        modified = datetime.strptime(modified_str,
                                     '%Y-%m-%dT%H:%M:%SZ')
        if not time_of_last_bike_change or\
                modified > time_of_last_bike_change:
            time_of_last_bike_change = modified

        # Does this location have any broken docks?
        broken_docks = docks - (bikes + spaces)

        # If location data wraps we've started a new collection.
        # So issue status messages...
        if uid < last_location_uid:
            if changes_since_last_collection == 0:
                # Nothing changed since the last connection.
                # Reassure the user we're still receiving data.
                _LOGGER.info('No activity.'
                             ' The most recent bike change was at %s',
                             time_of_last_bike_change)
            last_location_uid = uid
            changes_since_last_collection = 0

        last_bikes_at_this_location = -1
        if _USE_BIKE_CACHE:
            # Process the item if there's been a change
            # in the number of bikes...
            last_bikes_at_this_location = bike_cache.get(str(uid), -1)

        # Do we need to transmit this record?
        if not _USE_BIKE_CACHE or bikes != last_bikes_at_this_location:

            # Increment the count fo changes since the last block...
            changes_since_last_collection += 1

            # Change in bikes?
            # +ve means bikes at this location have just increased.
            # A -ve 'last value' implies this is the first set of data
            # for the given location.
            change = bikes
            if last_bikes_at_this_location > 0:
                change -= last_bikes_at_this_location
            if change > 0:
                change_str = f'+{change}'
            else:
                change_str = str(change)

            # Create an 'empty/full' marker.
            # If the number of bikes is 0 or the location is full
            # then mark the output somehow.
            marker = ' '
            if not spaces:
                marker = "!"
            elif not bikes:
                marker = '*'

            # Create simplified dictionary structure,
            # that we'll use to pass on to Chronicler.
            #
            # By defining an 'axis' (a _well known_ key)
            # we can employ axis-based pattern runners.
            bike_point_data = {'axis': uid,
                               'name': common_name,
                               'bikes': bikes,
                               'spaces': spaces,
                               'docks': docks,
                               'working docks': docks - broken_docks,
                               'broken docks': broken_docks,
                               'time': modified_str}
            _LOGGER.debug('%4s %s %s', change_str, marker, bike_point_data)

            # Now send to Chronicler?
            if chronicler:
                chronicler.post(bike_point_data)

            # Update our local database?
            if _USE_DB:
                # Insert new dock information
                # into the dock table...
                new_dock = Dock(bikes=bikes,
                                docks=docks,
                                empty_docks=spaces,
                                broken_docks=broken_docks,
                                modified=modified,
                                location=location_record,
                                collection=collection_record)
                db_session.add(new_dock)
                db_session.commit()

            if _USE_BIKE_CACHE:
                # Update the cache value for this site
                # Keys are strings
                bike_cache[str(uid)] = bikes

    _LOGGER.info('Leaving dispatch.')

    if _USE_DB:
        # Done with the DB session...
        _SESSION.remove()


def _get_data():
    """Calls the API to collect published data. In this case
    it's details about bike-point occupancy in the TfL network.

    Here we pass each record to the `_dispatch()` method via a Queue.

    If using a database we also populate the `Collection` table with
    the timing of each call.
    """
    # pylint: disable=global-statement
    global _LONGEST_API_DURATION

    # Timestamp the collection request time...
    collection_time = datetime.now()

    # Query the occupancy data across the network...
    success = True
    resp = None
    try:
        resp = requests.get(os.path.join(_API, _RESOURCE), params=_AUTH)
    except requests.exceptions.ConnectionError:
        success = False
    if not resp or not success:
        msg = 'TfL connection failure'
        _LOGGER.warning(msg)
        write_error(msg)
        return

    # How long did collection take?
    elapsed_time = datetime.now() - collection_time
    # Log if that was the longest...
    if elapsed_time > _LONGEST_API_DURATION:
        _LONGEST_API_DURATION = elapsed_time
        _LOGGER.info('New longest API call duration (%s)',
                     _LONGEST_API_DURATION)

    if _USE_DB:

        # Create a nwe DB session
        # to record the collection details
        # (time of request and its duration)
        db_session = _SESSION()

        # Add a new record to the collection table...
        new_collection = Collection(timestamp=collection_time,
                                    latency_s=elapsed_time.total_seconds())
        db_session.add(new_collection)
        db_session.commit()
        collection_record = db_session.query(Collection) \
            .filter(Collection.timestamp == collection_time).first()

        # Done with this thread's DB session...
        _SESSION.remove()

        # Put the collection record on the queue
        # (to be picked up by the dispatch method).
        # This record is used as a cross-reference in any potential
        # Location and BikePoint records that wil be made.
        _DISPATCH_QUEUE.put(collection_record)

    # Break-down the response into individual dictionary items and
    # pass them to the 'dispatch' thread using the dispatch queue.

    # For each record...
    data = []
    try:
        data = resp.json()
    except json.decoder.JSONDecodeError as json_e:
        msg = f'JSONDecodeError with TfL response ({json_e})'
        _LOGGER.warning(msg)
        write_error(msg)

    for item in data:
        _DISPATCH_QUEUE.put(item)


def _synchronise():
    """Pauses, synchronising collection to the System clock so that
    collections occur 'on the hour'. This is normally only called
    if the collection period is more than 1 minute.

    We assume that we are protected from collection periods that are not
    divisors of 60, i.e. 9.
    """

    # If the collection period is less than a minute
    # then there's nothing for us to do.
    if _COLLECTION_PERIOD_M <= 1:
        return

    _LOGGER.info('Synchronising collection to system clock...')

    # Schedule the collection of data so that it synchronises
    # (approximately) to the hour. Collection will be no faster than
    # one per minute - so wait until roughly between minutes...
    _LOGGER.info('Waiting for mid-minute...')
    current_time = datetime.now()
    while _RUN and current_time.second <= 24 \
            or current_time.second >= 36:
        time.sleep(_SHORT_SLEEP_S)
        current_time = datetime.now()

    # Imn the meantime the user may have hti CTRL-C,
    # in which case _RUN will be false. So only stay here
    # if we're still required to run.
    if _RUN:

        _LOGGER.info('At mid-minute.')

        # Now, how long do we need to wait so that we
        # start our collection job so that it fires 'on the hour'?
        current_minute = current_time.minute
        error = _COLLECTION_PERIOD_M - current_minute % _COLLECTION_PERIOD_M
        if error < _COLLECTION_PERIOD_M:
            msg = 'minute'
            if error > 1:
                msg += 's'
            sync_hour = current_time.hour
            sync_minute = current_minute + error
            if sync_minute >= 60:
                sync_hour += 1
            _LOGGER.info('Waiting for %d %s (until %02d:%02d)...',
                         error, msg, sync_hour, sync_minute % 60)
            while _RUN and (datetime.now() - current_time) \
                    .total_seconds() < error * 60:
                time.sleep(_SHORT_SLEEP_S)

        _LOGGER.info('Synchronised.')


def run():
    """The BikePoint collection entry point.

    We wait, synchronising to the hour and then start the collection process
    using a background scheduler.

    This call blocks until the collection is stopped.
    """

    if _APP_ID and _APP_KEY:

        # Licence requires us to acknowledge TfL and Ordnance Survey.
        # Make sure this is emitted when the code runs.
        _LOGGER.info('----------------------------')
        _LOGGER.info('TfL BikePoint Data Collector')
        _LOGGER.info('----------------------------')
        _LOGGER.info('Powered by TfL Open Data')
        _LOGGER.info('Contains OS data'
                     ' © Crown copyright and database rights 2016')

        # Expose useful config...
        _LOGGER.info('------------------')
        _LOGGER.info('Hit CTRL-C to quit')
        _LOGGER.info('------------------')
        _LOGGER.info('_CHRONICLER_URL="%s"', _CHRONICLER_URL)
        _LOGGER.info('_CHRONICLER_RESOURCE="%s"', _CHRONICLER_RESOURCE)
        _LOGGER.info('_COLLECTION_PERIOD_M="%s"', _COLLECTION_PERIOD_M)
        _LOGGER.info('_USE_BIKE_CACHE="%s"', _USE_BIKE_CACHE)
        _LOGGER.info('_USE_DB="%s"', _USE_DB)
        _LOGGER.info('------------------')

        # Attach a signal handler (for CTRL-C)
        signal.signal(signal.SIGINT, _signal_handler)

        # Synchronise collection so that it occurs 'on the hour'.
        _synchronise()

        # Continue
        # (unless user has already hit CTRL-C)
        if _RUN:

            bike_cache = None
            if _USE_BIKE_CACHE:
                # A cache of the current number of bikes at each site.
                # Used to allow the notification of changes.
                # Indexed by common name.
                bike_cache = shelve.open(_OCCUPANCY_CACHE_NAME, writeback=True)

            # Create a scheduler to trigger our collection method,
            scheduler = BackgroundScheduler()
            scheduler.start()

            # Create a Chronicler instance?
            chronicler = None
            if _CHRONICLER_URL:
                chronicler = ChroniclerTransmitter(_CHRONICLER_URL,
                                                   _CHRONICLER_RESOURCE,
                                                   _CHRONICLER_APP_ID,
                                                   _CHRONICLER_APP_CODE)

            # Create dispatcher thread
            dispatch_thread = Thread(target=_dispatch,
                                     name="BikePoint-Dispatcher",
                                     args=(_DISPATCH_QUEUE,
                                           bike_cache,
                                           chronicler))
            dispatch_thread.start()

            # Add a suitable job to the scheduler...
            _LOGGER.info('Starting %d-minute scheduler...',
                         _COLLECTION_PERIOD_M)
            scheduler.add_job(_get_data, 'interval',
                              minutes=_COLLECTION_PERIOD_M)

            _LOGGER.info('Running one pre-scheduler collection...')
            # And fire a collection of data now
            # (it is collection time after all)
            # and the schedule will wait another _POLL_PERIOD_MINUTES.
            _get_data()

            # Now wait for the dispatcher thread...
            _LOGGER.info('Running (with background scheduler).')

            _LOGGER.info('Joining dispatch thread.'
                         ' See you on the other side!')
            dispatch_thread.join()

            # Stop everything we started...
            scheduler.shutdown()
            if _USE_BIKE_CACHE:
                bike_cache.close()

        _LOGGER.info("That's all Folks!")
