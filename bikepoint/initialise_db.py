#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""Initialises a test database for the
TFL bike-point availability data collector.
"""

import enum
import os

from sqlalchemy import Column, DateTime, Enum, Float,\
    ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

BASE = declarative_base()

# The environment variable used to enable the use of the DB...
USE_DB_ENV = 'GARTEN_USE_DB'
DB = 'sqlite:///bikepoint.sqlite'


class PlaceType(enum.Enum):
    """Enumerations for the BikePoint PlaceType. Not sure what other values
    this field can have, the only one seen so far is BikePoint.
    """

    BIKE_POINT = enum.auto()


class Collection(BASE):
    """The TFL Collection table, details about individual calls to the API.
    Basically records the time the API was called and the response latency.
    """

    __tablename__ = 'collection'

    timestamp = Column(DateTime(timezone=True), primary_key=True)
    latency_s = Column(Float, nullable=False)

    def __repr__(self):
        return f"<Collection(timestamp='{self.timestamp}')>"


class Location(BASE):
    """The TFL Location table, details about individual locations.
    Locations have a string 'id' and common name and positional data.
    """

    __tablename__ = 'location'

    uid = Column(Integer, nullable=False, primary_key=True)
    common_name = Column(String(64), nullable=False)
    place_type = Column(Enum(PlaceType), nullable=False)
    lat = Column(Float, nullable=False)
    lon = Column(Float, nullable=False)

    collection_identity = Column(Integer, ForeignKey('collection.timestamp'))
    collection = relationship(Collection)

    def __repr__(self):
        return f"<Location(uid='{self.uid}')>"


class Dock(BASE):
    """The TFL Dock table, details about docks at specific locations.
    """

    __tablename__ = 'dock'

    identity = Column(Integer, autoincrement=True, primary_key=True)

    docks = Column(Integer, nullable=False)
    bikes = Column(Integer, nullable=False)
    empty_docks = Column(Integer, nullable=False)
    broken_docks = Column(Integer, default=0)
    modified = Column(DateTime(timezone=True))

    collection_identity = Column(Integer, ForeignKey('collection.timestamp'))
    collection = relationship(Collection)

    location_identity = Column(Integer, ForeignKey('location.uid'))
    location = relationship(Location)

    def __repr__(self):
        return f"<Dock(id='{self.identity}')>"


def run():
    """Entry method, initialised the DB.
    """

    if os.environ.get(USE_DB_ENV):

        # Create an engine that stores data
        # in the local directory's sqlite file.
        engine = create_engine(DB)

        # Create all tables in the engine.
        # This is equivalent to "Create Table"
        # statements in raw SQL.
        BASE.metadata.create_all(engine)
