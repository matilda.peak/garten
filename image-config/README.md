---
Title:      The Garten Docker Configuration File Directory
Author:     Alan Christie
Date:       30 February 2018
Copyright:  Matilda Peak. All rights reserved.
---

# image-config
This directory contains files for the production-grade
container image. They are copied to the image during the Docker build.
Files include...

-   `logging.yml`
