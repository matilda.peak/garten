# Matilda Peak modules...
matildapeak-chronicler-transmitter >= 2019.2

# Community modules...
apscheduler == 3.9.1
pytz == 2022.1
PyYAML == 5.4.1
requests == 2.27.1
sentry-sdk == 1.5.12
sqlalchemy == 1.4.36
