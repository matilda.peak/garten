#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

set -e

export TFL_APP_ID="Insert your TFL APP ID"
export TFL_APP_KEY="Insert your TfL application key"

export GARTEN_CHRONICLER_URL="Insert your Chronicler URL (and port)"

export GARTEN_USE_BIKE_CACHE=Yes
export GARTEN_COLLECTION_PERIOD_M=5
