#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

"""`Garten` is an experimental real-time data collector that is used to provide
demo material that can be used to build, experiment with and demonstrate
Chronicler.
"""

import os

import sentry_sdk

from bikepoint import initialise_db, bikepoint

# Initialise sentry SDK?
SENTRY_DSN = os.environ.get('GARTEN_SENTRY_DSN')
if SENTRY_DSN:
    sentry_sdk.init(SENTRY_DSN)

initialise_db.run()
bikepoint.run()
