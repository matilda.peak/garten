#!/bin/sh

# -----------------------------------------------------------------------------
# Copyright (C) 2019 Matilda Peak - All Rights Reserved.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# Proprietary and confidential.
# -----------------------------------------------------------------------------

# Health-check - return 0 if healthy, 1 otherwise.
#
# Garten writes a 'garten.error' file with error details if something has
# gone seriously wrong. If this file exists the health-check will return a
# non-zero error code.

if [ -f garten.error ]; then
    exit 1
fi

# Healthy if we get here...
exit 0
